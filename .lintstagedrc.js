module.exports = {
    // Type check TypeScript files
    "src/**/*.(ts)": () => "yarn tsc --noEmit",

    // Lint then format TypeScript and JavaScript files
    "src/**/*.(ts|js)": [
        "yarn eslint --fix --quiet",
        "yarn prettier --write",
    ],
};
