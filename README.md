# Treasure quest

Compute the outcomming of a treasure quest adventure from the given input

## Installation

```sh
npm i
# or
yarn install
```

## Usage example

[`./src/__fixtures__/input_example.txt`](./src/__fixtures__/input_example.txt)

```sh
# {C comme Carte} - {Nb. de case en largeur} - {Nb. de case en hauteur}
C - 3 - 4
# {M comme Montagne} - {Axe horizontal} - {Axe vertical}
M - 1 - 0
M - 2 - 1
#  {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors}
T - 0 - 3 - 2
T - 1 - 3 - 3
# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement}
A - Lara - 1 - 1 - S - AADADAGGA
```

```sh
# Replace the input file path to with your input file path
yarn start [input file path] [output file path (optionnal)]

# Example
yarn start ./src/__fixtures__/input_example.txt
```

Output:

`a.out`

```
C - 3 - 4
M - 1 - 0
M - 2 - 1
T - 1 - 3 - 2
A - Lara - 0 - 3 - S - 3
```
