import * as fs from "fs/promises";
import { TreasureQuest } from "./TreasureQuest";

const defaultOutputFile = "a.out";

async function main(inputFile: string, outPutFile = defaultOutputFile) {
  const fileContent = await fs.readFile(inputFile);
  const inputLines = fileContent.toString().split("\n");
  const treasureQuest = new TreasureQuest();
  treasureQuest.parseInputs(inputLines);
  treasureQuest.startQuest();
  await fs.writeFile(outPutFile, treasureQuest.toString());
}

const [inputFilePath, outputFile] = process.argv.slice(2);
main(inputFilePath, outputFile);
