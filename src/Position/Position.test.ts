import { Position } from ".";

describe("position", () => {
  it("is equal if x and y are equal", () => {
    const a = new Position(1, 2);
    const b = new Position(2, 1);
    const c = new Position(1, 2);
    expect(a.equals(b)).toBe(false);
    expect(a.equals(c)).toBe(true);
    expect(c.equals(b)).toBe(false);
  });
});
