import type { Adventurer } from "../Adventurer";
import type { Position } from "../Position";
import type { Space } from "../Spaces/Space";

export class TreasureMap {
  private _width: number;
  private _height: number;
  private _spaces: Space[] = [];

  public constructor(width: number, height: number) {
    this._width = width;
    this._height = height;
  }

  public addSpaces(...spaces: Space[]) {
    this._spaces.push(...spaces);
  }

  public at(pos: Position) {
    return this._spaces.find((e) => e.pos.equals(pos));
  }

  public canMoveAt(pos: Position): boolean {
    return (
      pos.x >= 0 &&
      pos.x < this._width &&
      pos.y >= 0 &&
      pos.y < this._height &&
      !this._spaces.some(
        (space) => space.pos.equals(pos) && !space.isPassable()
      )
    );
  }

  public enterInSpace(adventurer: Adventurer) {
    this._spaces.forEach((space, i, arr) => {
      if (
        space.pos.x === adventurer.pos.x &&
        space.pos.y === adventurer.pos.y
      ) {
        space.onEnter(adventurer);
      }
      if (space.shouldBeRemoved()) {
        arr.splice(i, 1);
      }
    });
  }

  public toString() {
    return [
      `C - ${this._width} - ${this._height}`,
      ...this._spaces.map((space) => space.toString()),
    ].join("\n");
  }
}
