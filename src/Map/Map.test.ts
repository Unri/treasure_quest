import { Adventurer } from "../Adventurer";
import { Orientation } from "../Adventurer/Orientation";
import { Position } from "../Position";
import { MountainSpace } from "../Spaces/Mountain";
import { TreasureMap } from "./Map";

describe("map", () => {
  it("can append and get spaces", () => {
    const map = new TreasureMap(10, 6);
    const space = new MountainSpace(1, 1);
    map.addSpaces(space);
    expect(map.at(new Position(1, 1))).toBe(space);
    expect(map.at(new Position(0, 0))).toBeUndefined();
  });

  it("can be converted to string", () => {
    const map = new TreasureMap(10, 6);
    map.addSpaces(
      new MountainSpace(1, 1),
      new MountainSpace(0, 1),
      new MountainSpace(0, 0)
    );
    expect(map.toString()).toBe(
      ["C - 10 - 6", "M - 1 - 1", "M - 0 - 1", "M - 0 - 0"].join("\n")
    );
  });

  it("check if one can move on specific position", () => {
    const map = new TreasureMap(2, 2);
    const mountain = new MountainSpace(1, 1);
    map.addSpaces(mountain);
    expect(map.canMoveAt(mountain.pos)).toBe(false);
    expect(map.canMoveAt(new Position(0, 1))).toBe(true);
  });

  it("cannot move out of the map bounds", () => {
    const map = new TreasureMap(2, 2);
    expect(map.canMoveAt(new Position(-1, 1))).toBe(false);
    expect(map.canMoveAt(new Position(1, -1))).toBe(false);
    expect(map.canMoveAt(new Position(2, 1))).toBe(false);
    expect(map.canMoveAt(new Position(1, 2))).toBe(false);
  });

  it("trigger on enter space", () => {
    const map = new TreasureMap(2, 2);
    const space = new MountainSpace(0, 0);
    const spy = jest.spyOn(space, "onEnter");
    map.addSpaces(space);
    const adventurer = new Adventurer("Jo", 0, 0, Orientation.South);
    map.enterInSpace(adventurer);
    expect(spy).toHaveBeenCalledWith(adventurer);
  });
});
