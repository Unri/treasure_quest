import { Position } from "../Position";
import type { Adventurer } from "../Adventurer";

export abstract class Space {
  public pos: Position;

  public constructor(x: number, y: number) {
    this.pos = new Position(x, y);
  }

  public abstract onEnter(adventurer: Adventurer): void;

  public abstract toString(): string;

  public isPassable(): boolean {
    return true;
  }

  public shouldBeRemoved(): boolean {
    return false;
  }
}
