import { Adventurer } from "../../Adventurer";
import { Orientation } from "../../Adventurer/Orientation";
import { Position } from "../../Position";
import { TreasureSpace } from ".";

describe("treasure space", () => {
  it("has T alias", () => {
    const treasureSpace = new TreasureSpace(2, 4, 2);
    expect(treasureSpace.pos).toStrictEqual(new Position(2, 4));
  });

  it("is passable", () => {
    const treasureSpace = new TreasureSpace(0, 0, 0);
    expect(treasureSpace.isPassable()).toBe(true);
  });

  it("print to string", () => {
    const treasureSpace = new TreasureSpace(1, 2, 1);
    expect(treasureSpace.toString()).toBe("T - 1 - 2 - 1");
  });

  it("has treasure count", () => {
    const treasureSpace = new TreasureSpace(0, 0, 4);
    expect(treasureSpace.treasureCount).toBe(4);
  });

  it("give one treasure when an adventurer enter the space", () => {
    const adventurer = new Adventurer("Roger", 0, 0, Orientation.East);
    const treasureSpace = new TreasureSpace(0, 0, 4);
    treasureSpace.onEnter(adventurer);
    expect(treasureSpace.treasureCount).toBe(3);
    expect(adventurer.treasureCount).toBe(1);
  });

  it("should not be removed if the treasure count is not 0", () => {
    const treasureSpace = new TreasureSpace(0, 0, 4);
    expect(treasureSpace.shouldBeRemoved()).toBe(false);
  });

  it("should be removed if the treasure count is 0", () => {
    const treasureSpace = new TreasureSpace(0, 0, 0);
    expect(treasureSpace.shouldBeRemoved()).toBe(true);
  });
});
