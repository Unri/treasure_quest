import { Space } from "../Space";
import type { Adventurer } from "../../Adventurer";

export class TreasureSpace extends Space {
  private _treasureCount: number;

  public constructor(x: number, y: number, treasureCount: number) {
    super(x, y);
    this._treasureCount = treasureCount;
  }

  public get treasureCount(): number {
    return this._treasureCount;
  }

  public onEnter(adventurer: Adventurer): void {
    adventurer.gainTreasure();
    this._treasureCount--;
  }

  public shouldBeRemoved(): boolean {
    return this._treasureCount === 0;
  }

  public toString(): string {
    return `T - ${this.pos.x} - ${this.pos.y} - ${this._treasureCount}`;
  }
}
