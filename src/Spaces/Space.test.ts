import { Space } from "./Space";

class TestSpace extends Space {
  public onEnter(): void {
    return;
  }
  public toString(): string {
    return ".";
  }
}

describe("space", () => {
  it("isPassable by default", () => {
    const testSpace = new TestSpace(1, 1);
    expect(testSpace.isPassable()).toBe(true);
  });

  it("should not be removed by default", () => {
    const testSpace = new TestSpace(1, 1);
    expect(testSpace.shouldBeRemoved()).toBe(false);
  });
});
