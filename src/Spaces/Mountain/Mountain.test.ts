import { Position } from "../../Position";
import { MountainSpace } from ".";

describe("mountain space", () => {
  it("has M alias", () => {
    const mountain = new MountainSpace(2, 4);
    expect(mountain.pos).toStrictEqual(new Position(2, 4));
  });

  it("is not passable", () => {
    const mountain = new MountainSpace(0, 0);
    expect(mountain.isPassable()).toBe(false);
  });

  it("print to string", () => {
    const mountain = new MountainSpace(1, 2);
    expect(mountain.toString()).toBe("M - 1 - 2");
  });

  it("should never be removed", () => {
    const mountain = new MountainSpace(1, 2);
    expect(mountain.shouldBeRemoved()).toBe(false);
  });
});
