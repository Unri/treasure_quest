import { Space } from "../Space";

export class MountainSpace extends Space {
  public constructor(x: number, y: number) {
    super(x, y);
  }

  public override isPassable(): boolean {
    return false;
  }

  public onEnter(): void {
    return;
  }

  public toString(): string {
    return `M - ${this.pos.x} - ${this.pos.y}`;
  }
}
