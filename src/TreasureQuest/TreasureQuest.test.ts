import { TreasureQuest } from ".";

const treasureQuestToString = (input: string[]) => {
  const treasureQuest = new TreasureQuest();
  treasureQuest.parseInputs(input);
  treasureQuest.startQuest();
  return treasureQuest.toString();
};

describe("treasure quest", () => {
  describe("individual parsing", () => {
    it("output the treasure quest map", () => {
      const input = ["C - 4 - 3"];
      expect(treasureQuestToString(input)).toBe("C - 4 - 3");
    });

    it("ignore invalid map inputs", () => {
      const input = ["C - Z - !", "C - 9", "C - 3 - 5 - 13", "C - 1 - 2"];
      expect(treasureQuestToString(input)).toBe("C - 1 - 2");
    });

    it("ignore comments", () => {
      const input = ["# some comment", "C - 1 - 2"];
      expect(treasureQuestToString(input)).toBe("C - 1 - 2");
    });

    it("output added mountains", () => {
      const input = ["C - 4 - 3", "M - 0 - 0", "M - 2 - 1"];
      expect(treasureQuestToString(input)).toStrictEqual(
        ["C - 4 - 3", "M - 0 - 0", "M - 2 - 1"].join("\n")
      );
    });

    it("ignore invalid mountains inputs", () => {
      const input = [
        "C - 4 - 3",
        "M - P - !",
        "M - 8",
        "M - 3 - 5 - 19",
        "M - 1 - 1",
      ];
      expect(treasureQuestToString(input)).toStrictEqual(
        ["C - 4 - 3", "M - 1 - 1"].join("\n")
      );
    });

    it("output added treasure space", () => {
      const input = ["C - 4 - 3", "T - 0 - 0 - 0", "T - 2 - 1 - 2"];
      expect(treasureQuestToString(input)).toStrictEqual(
        ["C - 4 - 3", "T - 0 - 0 - 0", "T - 2 - 1 - 2"].join("\n")
      );
    });

    it("ignore invalid treasure inputs", () => {
      const input = [
        "C - 4 - 3",
        "T - P - !",
        "T - 8",
        "T - 3 - 5",
        "T - 1 - 1 - 2",
      ];
      expect(treasureQuestToString(input)).toStrictEqual(
        ["C - 4 - 3", "T - 1 - 1 - 2"].join("\n")
      );
    });

    it("output added adventurer", () => {
      const input = ["C - 4 - 3", "A - Lara - 1 - 1 - S - D"];
      expect(treasureQuestToString(input)).toStrictEqual(
        ["C - 4 - 3", "A - Lara - 1 - 1 - O - 0"].join("\n")
      );
    });

    it("ignore invalid adventurer inputs", () => {
      const input = [
        "C - 4 - 3",
        "A - 4 - 3",
        "A - Po - 2",
        "A - =# - 1 - 3",
        "A - Yo - 1 - 2 - S - G",
      ];
      expect(treasureQuestToString(input)).toStrictEqual(
        ["C - 4 - 3", "A - Yo - 1 - 2 - E - 0"].join("\n")
      );
    });
  });

  describe("complete cases", () => {
    it("easy case", () => {
      const input = [
        "C - 3 - 4",
        "M - 1 - 0",
        "M - 2 - 1",
        "T - 0 - 3 - 2",
        "T - 1 - 3 - 3",
        "A - Lara - 1 - 1 - S - AADADAGGA",
      ];
      expect(treasureQuestToString(input)).toStrictEqual(
        [
          "C - 3 - 4",
          "M - 1 - 0",
          "M - 2 - 1",
          "T - 1 - 3 - 2",
          "A - Lara - 0 - 3 - S - 3",
        ].join("\n")
      );
    });

    it("has adventurers colliding", () => {
      const input = [
        "C - 2 - 2",
        "A - Bob - 0 - 0 - E - A",
        "A - Lemon - 1 - 0 - S - A",
      ];
      expect(treasureQuestToString(input)).toStrictEqual(
        [
          "C - 2 - 2",
          "A - Bob - 0 - 0 - E - 0",
          "A - Lemon - 1 - 1 - S - 0",
        ].join("\n")
      );
    });

    it("has adventurers with different number of moves", () => {
      const input = [
        "C - 2 - 2",
        "A - John - 0 - 0 - S - AGAGA",
        "A - Fabien - 1 - 0 - O - A",
      ];
      expect(treasureQuestToString(input)).toStrictEqual(
        [
          "C - 2 - 2",
          "A - John - 1 - 0 - N - 0",
          "A - Fabien - 0 - 0 - O - 0",
        ].join("\n")
      );
    });
  });

  it("adventurers only gain treasure when entering the space", () => {
    const input = [
      "C - 2 - 2",
      "T - 0 - 1 - 3",
      "A - Rat - 0 - 0 - S - ADDDDGGGGGA",
    ];
    expect(treasureQuestToString(input)).toStrictEqual(
      ["C - 2 - 2", "T - 0 - 1 - 2", "A - Rat - 1 - 1 - E - 1"].join("\n")
    );
  });
});
