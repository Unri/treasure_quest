import { Adventurer } from "../Adventurer";
import { Action } from "../Adventurer/Action";
import { moveForward } from "../Adventurer/Orientation";
import { TreasureMap } from "../Map/Map";
import { MountainSpace } from "../Spaces/Mountain";
import { TreasureSpace } from "../Spaces/Treasure";
import {
  parseAdventurerInputLine,
  parseMapInputLine,
  parseMountainInputLine,
  parseTreasureInputLine,
} from "./parsing-utils";

export class TreasureQuest {
  private _map: TreasureMap = new TreasureMap(0, 0);
  private _adventurerList: { adventurer: Adventurer; actions: Action[] }[] = [];

  public parseInputs(inputLines) {
    for (const line of inputLines) {
      switch (line[0]) {
        case "C": {
          parseMapInputLine(line, ({ width, height }) => {
            this._map = new TreasureMap(+width, +height);
          });
          break;
        }
        case "M": {
          parseMountainInputLine(line, ({ x, y }) => {
            this._map.addSpaces(new MountainSpace(+x, +y));
          });
          break;
        }
        case "T": {
          parseTreasureInputLine(line, ({ x, y, treasureCount }) => {
            this._map.addSpaces(new TreasureSpace(+x, +y, +treasureCount));
          });
          break;
        }
        case "A": {
          parseAdventurerInputLine(
            line,
            ({ name, x, y, orientation, actionSequence }) => {
              this._adventurerList.push({
                adventurer: new Adventurer(name, +x, +y, orientation),
                actions: actionSequence.split("") as Action[],
              });
            }
          );
          break;
        }
        default:
          break;
      }
    }
  }

  public startQuest() {
    while (this._adventurerList.some(({ actions }) => actions.length > 0)) {
      for (const { adventurer, actions } of this._adventurerList) {
        if (actions.length === 0) {
          continue;
        }
        const action = actions.shift()!;
        if (action !== Action.forward) {
          adventurer.move(action);
          continue;
        }

        const newPosition = moveForward(adventurer.pos, adventurer.orientation);
        const isCollidingWithAdventurer = !this._adventurerList.some(
          ({ adventurer: { pos } }) =>
            pos.x === newPosition.x && pos.y === newPosition.y
        );
        if (this._map.canMoveAt(newPosition) && isCollidingWithAdventurer) {
          adventurer.move(Action.forward);
          this._map.enterInSpace(adventurer);
        }
      }
    }
  }

  public toString() {
    return [
      this._map.toString(),
      ...this._adventurerList.map(({ adventurer }) => adventurer.toString()),
    ].join("\n");
  }
}
