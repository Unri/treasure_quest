import type { Orientation } from "../Adventurer/Orientation";

const parseInputLine =
  <Args extends Record<string, string>>(regexp: RegExp) =>
  (inputline: string, callback: (args: Args) => void) => {
    const result = regexp.exec(inputline);
    if (result?.groups) {
      callback(result.groups as Args);
    }
  };

export const parseMapInputLine = parseInputLine<{
  width: string;
  height: string;
}>(/^C - (?<width>\d+) - (?<height>\d+)$/);

export const parseMountainInputLine = parseInputLine<{ x: string; y: string }>(
  /^M - (?<x>\d+) - (?<y>\d+)$/
);

export const parseTreasureInputLine = parseInputLine<{
  x: string;
  y: string;
  treasureCount: string;
}>(/^T - (?<x>\d+) - (?<y>\d+) - (?<treasureCount>\d+)$/);

export const parseAdventurerInputLine = parseInputLine<{
  name: string;
  x: string;
  y: string;
  orientation: Orientation;
  actionSequence: string;
}>(
  /^A - (?<name>\w+) - (?<x>\d+) - (?<y>\d+) - (?<orientation>[NESO]) - (?<actionSequence>[ADG]+)$/
);
