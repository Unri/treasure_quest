export enum Action {
  forward = "A",
  turnLeft = "G",
  turnRight = "D",
}
