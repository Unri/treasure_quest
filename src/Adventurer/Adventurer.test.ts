import { Position } from "../Position";
import { Action } from "./Action";
import { Orientation } from "./Orientation";
import { Adventurer } from ".";

describe("adventurer", () => {
  it("has name, position and orientation", () => {
    const adventurer = new Adventurer("Lara", 1, 2, Orientation.North);
    expect(adventurer.name).toBe("Lara");
    expect(adventurer.pos).toStrictEqual(new Position(1, 2));
    expect(adventurer.orientation).toBe(Orientation.North);
  });

  it("print to string", () => {
    const adventurer = new Adventurer("Lara", 1, 2, Orientation.North);
    expect(adventurer.toString()).toBe("A - Lara - 1 - 2 - N - 0");
  });

  it("can gain a treasure one by one", () => {
    const adventurer = new Adventurer("Lara", 1, 2, Orientation.North);
    adventurer.gainTreasure();
    expect(adventurer.treasureCount).toBe(1);
    adventurer.gainTreasure();
    expect(adventurer.treasureCount).toBe(2);
  });

  it.each`
    initial              | result
    ${Orientation.North} | ${Orientation.West}
    ${Orientation.East}  | ${Orientation.North}
    ${Orientation.South} | ${Orientation.East}
    ${Orientation.West}  | ${Orientation.South}
  `("$initial turn left to $result", ({ initial, result }) => {
    const adventurer = new Adventurer("Lara", 1, 2, initial);
    adventurer.move(Action.turnLeft);
    expect(adventurer.orientation).toBe(result);
    expect(adventurer.toString()).toBe(`A - Lara - 1 - 2 - ${result} - 0`);
  });

  it.each`
    initial              | result
    ${Orientation.North} | ${Orientation.East}
    ${Orientation.East}  | ${Orientation.South}
    ${Orientation.South} | ${Orientation.West}
    ${Orientation.West}  | ${Orientation.North}
  `("$initial turn left to $result", ({ initial, result }) => {
    const adventurer = new Adventurer("Lara", 1, 2, initial);
    adventurer.move(Action.turnRight);
    expect(adventurer.orientation).toBe(result);
    expect(adventurer.toString()).toBe(`A - Lara - 1 - 2 - ${result} - 0`);
  });
  it.each`
    orientation          | x    | y
    ${Orientation.North} | ${1} | ${1}
    ${Orientation.East}  | ${2} | ${2}
    ${Orientation.South} | ${1} | ${3}
    ${Orientation.West}  | ${0} | ${2}
  `(
    "can move forward with orientation $orientation",
    ({ orientation, x, y }) => {
      const adventurer = new Adventurer("Lara", 1, 2, orientation);
      adventurer.move(Action.forward);
      expect(adventurer.pos).toStrictEqual(new Position(x, y));
      expect(adventurer.toString()).toBe(
        `A - Lara - ${x} - ${y} - ${orientation} - 0`
      );
    }
  );
});
