import { Position } from "../Position";

export enum Orientation {
  North = "N",
  East = "E",
  South = "S",
  West = "O",
}

export const turnLeftOrientation = (ori: Orientation): Orientation =>
  ({
    [Orientation.North]: Orientation.West,
    [Orientation.East]: Orientation.North,
    [Orientation.South]: Orientation.East,
    [Orientation.West]: Orientation.South,
  }[ori]);

export const turnRightOrientation = (ori: Orientation): Orientation =>
  ({
    [Orientation.North]: Orientation.East,
    [Orientation.East]: Orientation.South,
    [Orientation.South]: Orientation.West,
    [Orientation.West]: Orientation.North,
  }[ori]);

export const moveForward = ({ x, y }: Position, ori: Orientation): Position =>
  ({
    [Orientation.North]: new Position(x, y - 1),
    [Orientation.East]: new Position(x + 1, y),
    [Orientation.South]: new Position(x, y + 1),
    [Orientation.West]: new Position(x - 1, y),
  }[ori]);
