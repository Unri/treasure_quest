import { Position } from "../Position";
import { Action } from "./Action";
import {
  moveForward,
  turnLeftOrientation,
  turnRightOrientation,
} from "./Orientation";
import type { Orientation } from "./Orientation";

export class Adventurer {
  public readonly name: string;
  public pos: Position;
  public orientation: Orientation;

  private _treasureCount = 0;

  public constructor(
    name: string,
    x: number,
    y: number,
    orientation: Orientation
  ) {
    this.name = name;
    this.pos = new Position(x, y);
    this.orientation = orientation;
  }

  public get treasureCount(): number {
    return this._treasureCount;
  }

  public gainTreasure() {
    this._treasureCount++;
  }

  public move(action: Action) {
    switch (action) {
      case Action.forward: {
        this.pos = moveForward(this.pos, this.orientation);
        break;
      }
      case Action.turnLeft: {
        this.orientation = turnLeftOrientation(this.orientation);
        break;
      }
      case Action.turnRight: {
        this.orientation = turnRightOrientation(this.orientation);
        break;
      }
    }
  }

  public toString(): string {
    const { name, pos, orientation, _treasureCount } = this;
    return `A - ${name} - ${pos.x} - ${pos.y} - ${orientation} - ${_treasureCount}`;
  }
}
